FROM registry.gitlab.com/ytguidocker/gui/debian-bookworm-backports-xl:latest

RUN apt-get update \
  && apt-get install -yq xsltproc pdfarranger \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

# http://support.epson.net/linux/en/epsonscan2.php
#ADD https://download2.ebz.epson.net/epsonscan2/common/deb/x64/epsonscan2-bundle-6.7.42.0.x86_64.deb.tar.gz /tmp
ADD https://download2.ebz.epson.net/epsonscan2/common/deb/x64/epsonscan2-bundle-6.7.43.0.x86_64.deb.tar.gz /tmp
RUN cd /tmp; tar xfz *.tar.gz; cd epson*; ./install.sh


