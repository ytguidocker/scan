#!/bin/bash

image=registry.gitlab.com/ytguidocker/scan:latest

docker build --cache-from ${image} --tag ${image} .

startit(){
	myname=scan
	docker run  --rm \
		--name ${myname} \
		--hostname ${myname} \
		-e TZ=Europe/Berlin \
		${image}
}

startit

